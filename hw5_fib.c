#include "bigint.h"
#include <stdio.h>

void addbigsfibo(struct Bigint *a, struct Bigint b, struct Bigint c)
{
	a->biga=b.biga+c.biga;
	addOn(a,b.bigb);
	addOn(a,c.bigb);
}
int main(int argc, char *argv[])
{
	struct Bigint fib1;
	struct Bigint fib2;
	struct Bigint fib3;
	bigintinit(&fib1);
	bigintinit(&fib2);
	bigintinit(&fib3);
	addOn(&fib2,1);
	int i;
	for(i=2;i<51;i=i+1)
	{
		addbigsfibo(&fib3,fib2,fib1);
		copy(&fib1,fib2);
		copy(&fib2,fib3);
		bigintinit(&fib3);
	}
	printf("BigA=%d\tBigB=%d\n",fib2.biga,fib2.bigb);
}

