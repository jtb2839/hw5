#include <mpi.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
	int curr,total;
	double sum1=0;
	double sum2=0;
	MPI_Init(&argc,&argv);
	MPI_Comm_rank(MPI_COMM_WORLD,&curr);
	MPI_Comm_size(MPI_COMM_WORLD,&total);
	int start=10000*curr/total+1;
	int end=10000*(curr+1)/total;
	int i;
	double localsum1=0;
	double localsum2=0;
	for(i=start;i<(end+1);i++)
	{
		localsum1=localsum1+1.0/(i*i);
	}
	for(i=end;i>=start;i--)
	{
		localsum2=localsum2+1.0/(i*i);
	}
	MPI_Reduce(&localsum1,&sum1,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
	MPI_Reduce(&localsum2,&sum2,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
	if(curr==0)
	{
		printf("Sum increasing= %1.20f\tSum decreasing= %1.20f\n",sum1,sum2);
	}

	
}
