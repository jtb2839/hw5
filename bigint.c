#include "bigint.h"
#include <limits.h>
#include <stdio.h>
void bigintinit(struct Bigint *biggie)
{
	biggie->biga=0;
	biggie->bigb=0;
	biggie->mult=INT_MAX;
}
void addOn(struct Bigint *biggie, int addme)
{
	if(biggie->bigb>biggie->mult-addme)
	{
		biggie->biga=biggie->biga+1;
		biggie->bigb=addme-(biggie->mult-biggie->bigb);
	}
	else
	{
		biggie->bigb=biggie->bigb+addme;
	}
}
void copy(struct Bigint *a, struct Bigint b)
{
	a->biga=b.biga;
	a->bigb=b.bigb;
}

