CC:=gcc
CC2:=mpicc

hw5_sumbig: hw5_sumbig.o bigint.o
	$(CC) -o $@ $^ -lm
hw5_fib: hw5_fib.o bigint.o
	$(CC) -o $@ $^ -lm
hw5_sumsmall: hw5_sumsmall.o
	$(CC) -o $@ $^ -lm
hw5_sumsmall_mpi: hw5_sumsmall_mpi.o
	$(CC2) -o $@ $^
hw5_fib.o: hw5_fib.c bigint.h
	$(CC) -c $<
hw5_sumbig.o: hw5_sumbig.c bigint.h
	$(CC) -c $<
hw5_sumsmall.o: hw5_sumsmall.c
	$(CC) -c $<
hw5_sumsmall_mpi.o: hw5_sumsmall_mpi.c
	$(CC2) -c $<
bigint.o: bigint.c bigint.h
	$(CC) -c $<

all: hw5_fib hw5_sumbig hw5_sumsmall hw5_sumsmall_mpi
